extends GridContainer

export var line_width = 5
export(Color) var line_color
export(Color) var bg_color
export(Gradient) var line_gradient

var line = Line2D.new()
var sounds: AudioStreamPlayer
onready var heart_beeps = preload("res://assets/sounds/heart_rate.ogg")

export var x_label = ""
export var y_label = ""

export var x_ticks = 7
export var y_ticks = 7

var x_numerical = true
var y_numerical = true

var min_x
var min_y
var max_x
var max_y

var line_rect_width
var line_rect_height

var line_rect_x
var line_rect_y

var data = [
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0},
	{'x': 'SUN', 'y': 1.0},
	{'x': 'MON', 'y': 7.0},
	{'x': 'TUE', 'y': 8.0},
	{'x': 'WED', 'y': 3.0},
	{'x': 'THU', 'y': 5.0},
	{'x': 'FRI', 'y': 4.0},
	{'x': 'SAT', 'y': 6.0}
]

var data_clone: Array

var data_count: int = 0
var lines_to_show_in_graph: int = 42
var chart_size: Vector2

func _ready():
	$line_container/background.color = bg_color
	
	# check if values are numerical
	for val in data:
		if not [TYPE_INT, TYPE_REAL].has(typeof(val['x'])):
			x_numerical = false
		if not [TYPE_INT, TYPE_REAL].has(typeof(val['y'])):
			y_numerical = false
		
	# get min and max values (use index if value isn't a number, e.g. weekdays)
	for i in range(len(data)):
		var x_val = get_val(data[i]['x'], i)
		var y_val = get_val(data[i]['y'], i)
		
		if min_x == null or x_val < min_x:
			min_x = x_val
		if max_x == null or x_val > max_x:
			max_x = x_val
		if min_y == null or y_val < min_y:
			min_y = y_val
		if max_y == null or y_val > max_y:
			max_y = y_val
	
	# add tick labels to each axis
#	for i in range(x_ticks):
#		var x_tick = Label.new()
#		x_tick.size_flags_horizontal = SIZE_EXPAND_FILL
#		x_tick.align = HALIGN_CENTER
#		if x_numerical:
#			x_tick.text = str(round(i * (max_x-min_x) / (x_ticks-1) + min_x)) # optional rounding
#		else:
#			x_tick.text = str(data[i]['x'])
		#$x_ticks_container.add_child(x_tick)

#	for i in range(y_ticks-1, -1, -1):
#		var y_tick = Label.new()
#		y_tick.size_flags_vertical = SIZE_EXPAND_FILL
#		y_tick.valign = VALIGN_CENTER
#		if y_numerical:
#			y_tick.text = str(round(i * (max_y-min_y) / (y_ticks-1) + min_y)) # optional rounding
#		else:
#			y_tick.text = str(data[y_ticks-i-1]['y'])
		#$y_ticks_container.add_child(y_tick)
	
		# fix updated rect sizes not having correct values after altering labels
	yield(get_tree(), "idle_frame") or yield(VisualServer, "frame_post_draw")
	
	# shape the line
	line_rect_width = $line_container.rect_size.x
	line_rect_height = $line_container.rect_size.y
	
	line_rect_x = (line_rect_width / x_ticks)
	line_rect_y = (line_rect_height / y_ticks)
	
	line_rect_width = line_rect_x * (x_ticks-1)
	line_rect_height = line_rect_y * (y_ticks-1)
	
	var next_data_timer = Timer.new()
	next_data_timer.wait_time = 0.1
	next_data_timer.one_shot = false
	self.add_child(next_data_timer)
	next_data_timer.connect("timeout", self, "_on_next_data_timeout")
	next_data_timer.start()
	PrepareGraph()
	CopyData()
	
	# Establish the sound player
	sounds = AudioStreamPlayer.new()
	sounds.volume_db = -19
	add_child(sounds)
	PlayBeepSound()

func _on_next_data_timeout():
	# Called on each next_plot_data action
	DrawData()
	
func PrepareGraph():
	# generate line and apply style
	line.width = line_width
	line.default_color = line_color
	line.gradient = self.line_gradient
	
	$line_container.add_child(line)
		
func DrawData():
	# We need to pot the next item in the array	
	var i = data_clone.pop_front()
	if i != null:
		var scaled_x = scale_x(get_val(i['x'], data_count))
		var scaled_y = scale_y(get_val(i['y'], data_count))  + (rand_range(-15, 15) * $line_container.rect_size.x/900.0 ) # randf()  + randi()%50
		print("scaled_x: ", scaled_x, "scaled_y: ", scaled_y)
		line.add_point(Vector2(scaled_x, scaled_y))
		$BlipSprite.position = Vector2(scaled_x, scaled_y)
		data_count += 1
	else:
		ResetGraph()
		
	# Now let's remove a the first line point if we have a few lines
	print("line[0]: ", line.get_point_position(0))
	if line.get_point_count() > lines_to_show_in_graph:
		line.remove_point(0)

#	for i in range(len(data)):
#		var scaled_x = scale_x(get_val(data[i]['x'], i))
#		var scaled_y = scale_y(get_val(data[i]['y'], i))
#		line.add_point(Vector2(scaled_x, scaled_y))

func ResetGraph():
	# Clears the chart, fetches the data again
	line.clear_points()
	CopyData()
	data_count = 0
	PlayBeepSound()
	
func CopyData():
	# We don't want to be eating through our core data, so we make a copy of it for each pass, and eath the copy
	data_clone = data.duplicate()

func PlayBeepSound():
	# Plays the heart beep sonnd
	sounds.set_stream(heart_beeps)
	sounds.play()

func _input(event):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
		
func scale_x(val):
	var dx = max_x - min_x
	return ((val - min_x) * line_rect_width / dx) + line_rect_x/2


func scale_y(val):
	var dy = max_y - min_y
	return line_rect_height - ((val - min_y) * line_rect_height / dy) + line_rect_y/2


func get_val(val, idx):
	if [TYPE_INT, TYPE_REAL].has(typeof(val)):
		return val
	return idx
